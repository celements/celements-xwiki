package com.celements.common.classes;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.celements.init.CelementsStartedEvent;
import com.celements.model.util.ModelUtils;
import com.celements.wiki.WikiService;
import com.xpn.xwiki.XWikiConfigSource;

@Component
public class CheckClassesOnStart implements ApplicationListener<CelementsStartedEvent>, Ordered {

  private static final Logger LOGGER = LoggerFactory.getLogger(CheckClassesOnStart.class);

  private final XWikiConfigSource xwikiCfg;
  private final WikiService wikiService;
  private final IClassesCompositorComponent classesCompositor;
  private final ModelUtils modelUtils;

  @Inject
  public CheckClassesOnStart(
      XWikiConfigSource xwikiCfg,
      @Lazy WikiService wikiService,
      IClassesCompositorComponent classesCompositor,
      ModelUtils modelUtils) {
    this.xwikiCfg = xwikiCfg;
    this.wikiService = wikiService;
    this.classesCompositor = classesCompositor;
    this.modelUtils = modelUtils;
  }

  @Override
  public int getOrder() {
    return 100;
  }

  @Override
  public void onApplicationEvent(CelementsStartedEvent event) {
    LOGGER.trace("onApplicationEvent: {}", event);
    if ("1".equals(xwikiCfg.getProperty("celements.classCollections.checkOnStart", "1"))) {
      LOGGER.info("checking classes");
      wikiService.streamAllWikis()
          .filter(wiki -> !modelUtils.isMainWiki(wiki))
          .forEach(classesCompositor::checkClasses);
    }
  }

}
