/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.celements.mandatory;

import static com.celements.execution.XWikiExecutionProp.*;
import static java.util.stream.Collectors.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xwiki.component.annotation.Component;
import org.xwiki.component.annotation.Requirement;
import org.xwiki.context.Execution;
import org.xwiki.model.reference.WikiReference;

import com.celements.init.WikiUpdater;
import com.xpn.xwiki.XWikiContext;

@Component
public class MandatoryDocumentCompositor implements IMandatoryDocumentCompositorRole {

  private static final Logger LOGGER = LoggerFactory.getLogger(MandatoryDocumentCompositor.class);

  @Requirement
  Map<String, IMandatoryDocumentRole> mandatoryDocumentsMap;

  @Requirement
  private WikiUpdater wikiUpdater;

  @Requirement
  Execution execution;

  protected XWikiContext getContext() {
    return (XWikiContext) execution.getContext().getProperty("xwikicontext");
  }

  @Override
  public void checkAllMandatoryDocuments() {
    execution.getContext().get(WIKI)
        .ifPresent(this::checkAllMandatoryDocuments);
  }

  @Override
  public void checkAllMandatoryDocuments(WikiReference wikiRef) {
    if (wikiUpdater.isShutdown()) {
      checkAllMandatoryDocumentsForContext();
    } else {
      wikiUpdater.runUpdateAsync(wikiRef, this::checkAllMandatoryDocumentsForContext);
    }
  }

  private void checkAllMandatoryDocumentsForContext() {
    LOGGER.info("checkAllMandatoryDocuments for wiki [{}] ", getContext().getDatabase());
    for (String mandatoryDocKey : getMandatoryDocumentsList()) {
      IMandatoryDocumentRole mandatoryDoc = mandatoryDocumentsMap.get(mandatoryDocKey);
      try {
        LOGGER.trace("checkDocuments - starting [{}]", mandatoryDoc.getClass());
        mandatoryDoc.checkDocuments();
        LOGGER.debug("checkDocuments - done [{}]", mandatoryDoc.getClass());
      } catch (Exception exp) {
        LOGGER.warn("Failed checking mandatory documents for wiki [" + getContext().getDatabase()
            + "] and  component [" + mandatoryDoc.getClass() + "]", exp);
      }
    }
  }

  List<String> getMandatoryDocumentsList() {
    Collection<String> mandatoryDocElemKeys = mandatoryDocumentsMap.keySet().stream()
        .sorted((k1, k2) -> Integer.compare(
            mandatoryDocumentsMap.get(k1).order(),
            mandatoryDocumentsMap.get(k2).order()))
        .collect(toList());
    List<String> mandatoryDocExecList = new ArrayList<>();
    do {
      mandatoryDocElemKeys.stream()
          .filter(key -> mandatoryDocumentsMap.get(key)
              .dependsOnMandatoryDocuments().stream()
              .filter(mandatoryDocumentsMap::containsKey)
              .allMatch(mandatoryDocExecList::contains))
          .forEach(mandatoryDocExecList::add);
    } while (mandatoryDocElemKeys.removeAll(mandatoryDocExecList)
        && !mandatoryDocElemKeys.isEmpty());
    for (String skippedDocElemKey : mandatoryDocElemKeys) {
      LOGGER.error("Cannot order all mandatory document roles. Thus skipping: {}",
          skippedDocElemKey);
    }
    LOGGER.debug("getMandatoryDocumentsList returning [{}]", mandatoryDocExecList);
    return mandatoryDocExecList;
  }

}
